const body = document.querySelector('body');
const app = document.querySelector('#app');

function setStyles() {
    /*
    * El requisito es no poder usar CSS externo o inline en el HTML,
    * por ello agregamos el CSS mediante la propiedad cssText (para reducir el tamaño de
    * nuestro código) o también se puede hacer meidante app.style.Propiedad = 'valor CSS',
    * una línea de código por cada propiedad.
    */
    app.style.cssText = 'position: fixed; width: 100vw; height: 100vh; display: flex; align-items: center; justify-content: center;';

    /*
    * Y aquí está la trampa: crearemos un elemento <style></style>
    * Con este elemento podremos poner todo el CSS que queramos, por tanto,
    * podremos incluso ahorrarnos los estilos que hemos aplicado arriba al elemento "#app".
    * 
    * Ponemos las dos maneras por si alguien desea añadir la funcionalidad opcional "dark mode".
    * 
    * Nota: He importado "Montserrat" y el reset CSS para que se normalicen los estilos en todos
    * los navegadores. Del reset CSS he eliminado aquello que no nos afecta.
    */

    let styles = document.createElement('style');
    styles.innerHTML = `
    /* Importamos nuestra fuente */

    @import url('https://fonts.googleapis.com/css2?family=Montserrat:wght@900&display=swap');

    /* Metemos reset.css para evitar comportamientos extraños */

    /* http://meyerweb.com/eric/tools/css/reset/ 
        v2.0 | 20110126
        License: none (public domain)
    */
        html, body, div, h1 {
            margin: 0;
            padding: 0;
            border: 0;
            font-size: 100%;
            font: inherit;
            vertical-align: baseline;
        }

        /* Nuestros estilos */ 

        #app {
            font-size: 18px;
        }

        h1 {
            font-size: 8em;
            font-family: 'Montserrat', sans-serif;
            color: #29292e;
            cursor: pointer;
            user-select: none;
            letter-spacing: 20px;
        }

        @media (max-width: 900px) {
            h1 {
                font-size: 6em;
            }
        }

        @media (max-width: 768px) {
            h1 {
                font-size: 4em;
            }
        }

        @media (max-width: 500px) {
            h1 {
                font-size: 2em;
            }
        }

        body.dark {
            background-color: #2d2d2d;
        }
        body.dark h1 {
            color: #cdcfd3;
        }
    `;
    // Agregamos el elemento <style></style> a nuestro <head>
    document.getElementsByTagName('head')[0].appendChild(styles);
}

function init() {
    // Agregamos los estilos CSS
    setStyles();
    // Creamos el h1 que contendrá el tiempo
    let timeH1 = document.createElement('h1');

    // Le agreamos un evento "click" para agregar la clase "dark" al body
    timeH1.addEventListener('click', function (e) {
        e.preventDefault();
        body.classList.toggle('dark');
    });

    // Agregamos el h1 a nuestro #app
    app.appendChild(timeH1);

    // Para evitar comportamientos no deseados agregamos 0.5s en lugar de 1s
    setInterval(() => {
        let hoy = new Date();
        let horas = hoy.getHours().toString().padStart(2, '0');
        let minutos = hoy.getMinutes().toString().padStart(2, '0');
        let segundos = hoy.getSeconds().toString().padStart(2, '0');
    
        timeH1.innerHTML = `${horas}:${minutos}:${segundos}`;
    }, 500);
}

// Arrancamos nuestra App
init();
